import { Heroe } from './../../interfaces/hereo.interface';

import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HeroesService } from '../../services/heroes.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})

export class HeroeComponent  {
  heroe: Heroe  = {
    nombre: '',
    bio: '',
    casa: 'Marvel'
  };
  nuevo = false;
  id: string;

  constructor(private _heroeService: HeroesService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(
      patametros => {
        this.id = patametros['id'];

        if (this.id !== 'new') {
          this._heroeService
            .getHeroe(this.id)
            .subscribe(dataHeroe => (this.heroe = dataHeroe));

        }
      }
    );
  }

  guardar() {
    if (this.id === 'new') {
      // insertando
      this._heroeService.nuevoHeroe(this.heroe).subscribe(data => {
        // despoues de agregar al heroe navegamos a la otra pagina
        this.router.navigate(['/heroe', data.name]);
      }, error => console.log(error));
    } else {
      // actualizando
      this._heroeService.actualizarHeroe(this.heroe, this.id).subscribe(data => {
          // despoues de agregar al heroe navegamos a la otra pagina
          // this.router.navigate(['/heroe', data.name]);
          console.log(data);
        }, error => console.log(error));
    }
  }
  agregarHeroe(forma: NgForm) {
    this.router.navigate(['/heroe', 'new']);
    // reseteo campos , por defecto Marvel
    forma.reset({
      casa: 'Marvel'
    });
  }
}
