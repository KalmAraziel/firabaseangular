import { Heroe } from './../interfaces/hereo.interface';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  heroesURL = 'https://heroesapp-63455.firebaseio.com/heroes.json';
  heroeURL = 'https://heroesapp-63455.firebaseio.com/heroes/';

  constructor(private http: Http) {}

  nuevoHeroe(heroe: Heroe) {
    const body = JSON.stringify(heroe);
    const headers = new Headers({
      'Content-Type': 'applicatio/json'
    });
    return this.http.post(this.heroesURL, body, { headers }).map(res => {
      // console.log(res.json() );
      return res.json();
    });
  }

  actualizarHeroe(heroe: Heroe, key$: string) {
    const body = JSON.stringify(heroe);
    const headers = new Headers({
      'Content-Type': 'applicatio/json'
    });
    const url = `${this.heroeURL}/${key$}.json`;
    return this.http.put(url, body, { headers }).map(res => {
      // console.log(res.json() );
      return res.json();
    });
  }

  getHeroe(key$: string) {
    const url = `${this.heroeURL}/${key$}.json`;
    return this.http.get(url).map(res => res.json());
  }

  getHeroes() {
    const url = `${this.heroesURL}`;
    return this.http.get(url).map(res => res.json());
  }
  eliminarHeroe(key: string) {
      const url = `${this.heroeURL}/${ key }.json`;
      return this.http.delete(url).map(res => res.json());
  }
}
