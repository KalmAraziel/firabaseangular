
// import { Heroe } from './../../interfaces/hereo.interface';
import { HeroesService } from './../../services/heroes.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {
  heroes: any[] = [];
  loading = true;
  constructor(private _heroeService: HeroesService, private router: Router) {
    this._heroeService.getHeroes()
      .subscribe( data => {
        this.loading = false;
        this.heroes = data;
      });
  }

  ngOnInit() {}

  editar(key: string) {
    console.log(key);
    if ( key !== '' ) {
      this.router.navigate(['heroe', key]);
    }
  }

  eliminarHeroe(key: string) {
    this._heroeService.eliminarHeroe(key).subscribe(response => {
      // si esta bien no trae nada
      if (response) {
        console.error(response);
      } else {
        // todo bien y lo elimino del array
        delete this.heroes[key];
      }
    });
  }
}
